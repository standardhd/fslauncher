#############################################################
#
# Description: This Script will Read a CSV file Included In
# In the folder with the script. Currently Hard Coded to
# ".\programs.csv". 
# It will start the given programs in given order
#
#############################################################
#Make Script location the current folder
### Load Assembly
[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")

### PS-Version check ###

$MinVersion = 7
$PSversion = $PSVersionTable.PSVersion.Major


if ($PSversion -lt $MinVersion) {
   echo " Powershell 7 required"
   $msgboxresult = [System.Windows.Forms.MessageBox]::Show("PowerShell 7 required! Do you want to download Powershell 7 ?","PowerShell Version 7 required",3,[System.Windows.Forms.MessageBoxIcon]::Exclamation)
 
   If ($msgboxresult -eq "Yes")
   {
      Start-Process "https://docs.microsoft.com/de-de/powershell/scripting/install/installing-powershell-on-windows"
   }
   else
   {
      ### stop script 
      break
   }
}



Split-Path -parent $MyInvocation.MyCommand.Definition | Set-Location
clear
Write-Host -ForegroundColor Blue "

#############################################################################
#
# FS Launcher
# Latest Version: 0.5
# Version 0.5 / September 27 2022
# Author: Jens Stroede | mail@jensstroede.de
#
# This Script is provided without warranty of any kind.
# Use at your own discretion.
#
#
#
##############################################################################
"

$Objects = Import-Csv .\programs.csv  -delimiter ","

$Objects | ForEach-Object {

$Name = $_.Name
$Path = $_.Path
$Wait = $_.Wait
$Enabled = $_.Enabled
$ProcessName = Split-Path $Path -LeafBase

### Check if program is already running
### If its running skip it
### otherwise start it

if(Get-Process | Where-Object -Property Name -eq $ProcessName)
   {
   Write-Host $Name "is already running ... Skipping"
   Start-Sleep -Seconds 2
   }
   elseif ($Enabled -eq 1)
      {
   echo $Name" is starting"
   Start-Sleep -Seconds $Wait
   & $Path
      }
   else 
      {
   echo $Name" is disabled"
      }
}

Write-Host $Script
clear