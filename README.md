# FSLauncher

A powershell-script / program to start several programs after each other.


## Getting started

Download the powershell script and the csv-file.
The csv-file containts 3 rows Name , Path , Wait und Enabled

Fill out the csv-file with the nessessary programs / apps you want to start prior to your flight sim.
The programs will be startet in the order as given in the csv file.
The csv file is prefilled with some programs already.

## CSV-File Template
Name,Path,Wait,Enabled

Name = Name for your program
Path = Path to your program (e.g: "C:\Program Files\EFB\EFB.exe)
Wait = Time in seconds to wait before starting program
Enabled = posible Values: 1 (program will start) or 0 (program will not start)

## Description
Using tons of addons and programs for our flight sim hobby, you need to start several programs, e.g. SimToolkitPro or ActiveWeather and so on.
With this little helper script you can automate this task by simply fill out a csv-file and then let the programs startet automaticly in order given in the csv file.

## Contributing
Contribution (mainly a nice GUI would be great) are very welcome!

## Planned features
- Update-Check
- GUI 
    - Drag and Drop for sorting
    - Profiles (Xplane or MSFS or whatever)
    - Get Paths of wellknown sim programs
- maybe rewrite in python or swift to support MAC and Linux users

## Authors and acknowledgment
Author: Jens Stroede | mail@jensstroede.de
## License
Licenced under GPL V3
